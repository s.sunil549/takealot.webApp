package com.takealot.webapp.stepDef;

import org.openqa.selenium.WebDriver;

import com.takealot.common.CommonCV;
import com.takealot.webapp.pages.HomeDashboardPage;
import com.takealot.webapp.pages.RegisterPage;
import com.takealot.webapp.pages.SearchPage;
import com.takealot.webapp.pages.ShoppingCartPage;
import com.takealot.webapp.scenarioTests.TakealotWebAppTests;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SearchProductAndAddToCart extends CommonCV {

	private static RegisterPage registerPage;
	private static HomeDashboardPage homeDashboardPage;
	private static SearchPage searchPage;
	private static ShoppingCartPage shoppingCartPage;
	WebDriver driver;

	@Before
	public void init() {
		driver = TakealotWebAppTests.driver;
		registerPage = new RegisterPage(driver);
		homeDashboardPage = new HomeDashboardPage(driver);
		searchPage = new SearchPage(driver);
		shoppingCartPage = new ShoppingCartPage(driver);

	}

	@Given("^Launch browser and navigate to Takelot application$")
	public void launch_browser_and_navigate_to_Takelot_application() throws Throwable {
		driver.get(webURL);
	}

	@Given("^Navigate to Register User page$")
	public void navigate_to_Register_User_page() throws Throwable {
		homeDashboardPage.clickOnRegister();
	}

	@When("^Enter user details to register a user$")
	public void enter_user_details_to_register_a_user(DataTable userDetails) throws Throwable {
		registerPage.validateReistrationPage();
		registerPage.enterUserDetailsToRegister(userDetails);
	}

	@When("^click on Register button$")
	public void click_on_Register_button() throws Throwable {
		registerPage.clickOnRegisterBtn();
	}

	@Then("^validate successful registration of user$")
	public void validate_successful_registration_of_user() throws Throwable {
		registerPage.validateRegistrationSuccess(welcomeMsg, successMsg);
	}

	@When("^search for products and add to cart$")
	public void search_for_products_and_add_to_cart(DataTable products) throws Throwable {
		searchPage.searchProductBasedOnDept(products, department);
	}

	@Then("^click on Checkout$")
	public void click_on_Cart() throws Throwable {
		searchPage.clickOnCheckoutCartBtn();
	}

	@Then("^validate the added products in cart$")
	public void validate_the_added_card(DataTable addedProducts) throws Throwable {
		shoppingCartPage.validateShoppingCart(addedProducts);
		homeDashboardPage.clickOnLogout();
	}

}
