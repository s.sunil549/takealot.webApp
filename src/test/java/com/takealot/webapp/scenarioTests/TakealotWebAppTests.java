package com.takealot.webapp.scenarioTests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.takealot.common.SingletonDriver;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = "./src/test/resources/WebAppFeature", glue = { "com.takealot.webapp.stepDef" }, tags = {
"@SearchProductAndAddToCart" }, plugin = { "html:target/cucumber-html-report", "json:target/cucumber.json",
		"pretty:target/cucumber-pretty.txt", "junit:target/cucumber-results.xml" })
public class TakealotWebAppTests extends AbstractTestNGCucumberTests  {

	
	public static WebDriver driver;
	String browserType;
	
	public String getBrowserType() {
		return browserType;
	}
	public void setBrowserType(String browserType) {
		this.browserType = browserType;
	}


	@BeforeClass(alwaysRun = true)
	@Parameters({"browserType"})
	public void setUp(String browserType){
		
		setBrowserType(browserType);
		driver = SingletonDriver.getInstance(browserType).getDriver();
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}
	
	
	@AfterSuite(alwaysRun = true)
	public void teardown() {
		driver.close();
	}
	
	
	
}
