package com.takealot.api.stepDef;

import org.testng.Assert;
import org.testng.Reporter;

import com.takealot.api.DTO.CurrentWeatherDataDTO;
import com.takealot.api.Utils.CurrentWeatherDataAPI;

import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CurrentWeatherData {

	private static CurrentWeatherDataAPI currentWeatherDataAPI = null;
	CurrentWeatherDataDTO currentWeatherDataDTO = null;

	@Before
	public void init() {
		currentWeatherDataAPI = new CurrentWeatherDataAPI();
	}

	@When("^I fetch current weather data by \"([^\"]*)\",\"([^\"]*)\"$")
	public void i_fetch_current_weather_data_by(String param, String cityName) throws Throwable {

		currentWeatherDataDTO = currentWeatherDataAPI.currentWeatherMapByCity(param, cityName);
	}

	@Then("^current weather data for the city with (\\d+) ,\"([^\"]*)\" , \"([^\"]*)\" , \"([^\"]*)\" ,\"([^\"]*)\" ,\"([^\"]*)\" in step$")
	public void current_weather_data_for_the_city_with_in_step(int statusCode, String message, String city,
			String country, String latitude, String longitude) throws Throwable {

		if (!(message.isEmpty())) {
			Assert.assertEquals(currentWeatherDataDTO.getCod().intValue(), statusCode);
			Assert.assertEquals(currentWeatherDataDTO.getMessage().toString(), message);
		} else {
			Assert.assertEquals(currentWeatherDataDTO.getCod().intValue(), statusCode);
			Assert.assertEquals(currentWeatherDataDTO.getName().toString(), city);
			Assert.assertEquals(currentWeatherDataDTO.getSys().getCountry().toString(), country);
			Assert.assertEquals(currentWeatherDataDTO.getCoord().getLat(), Double.parseDouble(latitude));
			Assert.assertEquals(currentWeatherDataDTO.getCoord().getLon(), Double.parseDouble(longitude));

			Reporter.log("--> Sunrise : " + currentWeatherDataDTO.getSys().getSunrise().toString(), true);
			Reporter.log("--> Sunset : " + currentWeatherDataDTO.getSys().getSunset().toString(), true);
			Reporter.log("--> Temperature : " + currentWeatherDataDTO.getMain().getTemp().toString(), true);
			Reporter.log("--> Max. Temperature : " + currentWeatherDataDTO.getMain().getTempMax().toString(), true);
			Reporter.log("--> Min. Temperature : " + currentWeatherDataDTO.getMain().getTempMin().toString(), true);
			Reporter.log("--> Pressure : " + currentWeatherDataDTO.getMain().getPressure().toString(), true);
			Reporter.log("--> Humidity : " + currentWeatherDataDTO.getMain().getHumidity().toString(), true);
		}
		Reporter.log("TestCase : PASS", true);

	}
	
	@When("^I fetch current weather data by geographic coordinates \"([^\"]*)\",\"([^\"]*)\"$")
	public void i_fetch_current_weather_data_by_geographic_coordinates(String param1, String param2) throws Throwable {
		
		currentWeatherDataDTO = currentWeatherDataAPI.currentWeatherMapByGeographiCoordinates(param1, param2);
	}
	
	

}
