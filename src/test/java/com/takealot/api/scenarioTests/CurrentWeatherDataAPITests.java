package com.takealot.api.scenarioTests;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = "./src/test/resources/APIFeature", glue = { "com.takealot.api.stepDef" }, tags = {
		"@CurrentWeatherData" }, plugin = { "html:target/cucumber-html-report", "json:target/cucumber.json",
				"pretty:target/cucumber-pretty.txt", "junit:target/cucumber-results.xml" })
public class CurrentWeatherDataAPITests extends AbstractTestNGCucumberTests {

}
