@SearchProductAndAddToCart
Feature: Search Products & Add to Cart Test

  Background: 
    Given Launch browser and navigate to Takelot application

  @RegisterAUser
  Scenario: Register a user in Takelot Webapp
    Given Navigate to Register User page
    When Enter user details to register a user
      | Jamie | Lannister | s.sunil549+ | demoUser1 | 07709087623 |
    And click on Register button
    Then validate successful registration of user

  @SearchProducts
  Scenario: Search Products and Add to Cart
    When search for products and add to cart
      | Garmin Forerunner 45S Sports Smartwatch  |
      | Garmin QuickFit 22mm Silicone Watch Band |
    Then click on Checkout
    And validate the added products in cart
      | Garmin Forerunner 45S Sports Smartwatch  |
      | Garmin QuickFit 22mm Silicone Watch Band |
