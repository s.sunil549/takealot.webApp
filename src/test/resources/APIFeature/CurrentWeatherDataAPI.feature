#Author: Sunil.S
@CurrentWeatherData
Feature: Call Current Weather Data API for one location

@CurrentWeatherData-ByCityName
Scenario Outline:  Call Current Weather Data API for one location by City Name

	When I fetch current weather data by "<queryParam>","<cityName>"
	Then current weather data for the city with <statusCode> ,"<message>" , "<city>" , "<country>" ,"<latitude>" ,"<longitudue>" in step
 
 Examples:
 |queryParam|cityName    |statusCode|message           |city     |country|latitude|longitudue|
 |q					|            |400       |Nothing to geocode|         |       |        |          |
 |q         |abcd			   |404 			|city not found    |         |       |        |          |
 |q         |Bengaluru,IN|200 			|							     |Bengaluru|IN     |12.9762 |77.6033   |
 
 
 
@CurrentWeatherData-ByCityId
Scenario Outline:  Call Current Weather Data API for one location by City ID

	When I fetch current weather data by "<queryParam>","<cityName>"
	Then current weather data for the city with <statusCode> ,"<message>" , "<city>" , "<country>" ,"<latitude>" ,"<longitudue>" in step
 
 Examples:
 |queryParam|cityName    |statusCode|message              |city     |country|latitude|longitudue|
 |id				|            |400       |Nothing to geocode   |         |       |        |          |
 |id				|0           |400       |Invalid ID           |         |       |        |          |
 |id        |abcd			   |400 			|abcd is not a city ID|         |       |        |          |
 |id				|012         |404       |city not found       |         |       |        |          |
 |id        |1277333     |200 			|							        |Bengaluru|IN     |12.9762 |77.6033   |
 
 
 
@CurrentWeatherData-ByCityZipCode
Scenario Outline:  Call Current Weather Data API for one location by City ZipCode

	When I fetch current weather data by "<queryParam>","<cityName>"
	Then current weather data for the city with <statusCode> ,"<message>" , "<city>" , "<country>" ,"<latitude>" ,"<longitudue>" in step
 
 Examples:
 |queryParam|cityName    |statusCode|message              |city                   |country|latitude|longitudue|
 |zip				|            |400       |Nothing to geocode   |                       |       |        |          |
 |zip				|0           |400       |invalid zip code     |                       |       |        |          |
 |zip       |abcd			   |404 			|city not found       |                       |       |        |          |
 |zip       |560038,IN   |200 			|							        |Indiranagar (Bangalore)|IN     |12.9743 |77.6404   |
 
 
@CurrentWeatherData-ByGeographicCoordinates
Scenario Outline:  Call Current Weather Data API for one location by Geographic Coordinates

	When I fetch current weather data by geographic coordinates "<param1>","<param2>"
	Then current weather data for the city with <statusCode> ,"<message>" , "<city>" , "<country>" ,"<latitude>" ,"<longitudue>" in step
 
 Examples:
 |param1 |param2 |statusCode|message           |city                   |country|latitude|longitudue|
 |77.6404|12.9743|200 			|                  |Bengaluru              |IN     |12.9743 |77.6404   |

 

 

 
 