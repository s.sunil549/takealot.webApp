package com.takealot.common;

public class CommonCV {

	// Web Configuration Details
	public static String currentDir = System.getProperty("user.dir");
	public static String chromeDriverExecutablePath = currentDir + "/src/test/resources/DriverExecutable/chromedriver";
	public static String webURL = "https://www.takealot.com";
	
	// API Configuration Details
	public static String baseURL = "https://api.openweathermap.org";
	public static String basePath = "/data/2.5/weather";
	public static String appId = "a538b71511055e8608e1e7ab9f52d1fe";
	
	
	// Test Data for Registration of User
	public static String welcomeMsg = "Welcome to the TAKEALOT.com family!";
	public static String successMsg = "Thanks for registering with us, we reckon this is the start of a beautiful relationship.";
	
	
	// Test Data for Serach and Add to Cart Product
	public static String department = "Cellphones & Wearables";
	

	

}
