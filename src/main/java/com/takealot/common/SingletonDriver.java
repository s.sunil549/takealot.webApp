package com.takealot.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SingletonDriver extends CommonCV  {
	
	
	private static SingletonDriver driverClass = null;
	private WebDriver driver;
	
	// Constructor to instanticate driver
	public SingletonDriver(String browsertype){
		
		if(browsertype.equalsIgnoreCase("chrome")){
			WebDriverManager.chromedriver().setup();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			options.addArguments("--disable-notifications");
			options.addArguments("--disable-extenstions");
			options.addArguments("disable-infobars");
			driver = new ChromeDriver(options);
		}	
	}
	
	// Create Singleton Instance Driver
	public static SingletonDriver getInstance(String browserType){	
		if( driverClass == null){
			driverClass = new SingletonDriver(browserType);
		}
		return driverClass;
	}
	
	// Returning WebDriver instance
	public WebDriver getDriver(){
		return driver;
	}

}
