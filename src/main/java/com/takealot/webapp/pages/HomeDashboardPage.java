package com.takealot.webapp.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Reporter;

import com.takealot.webapp.Utils.ActionUtils;

import junit.framework.Assert;

public class HomeDashboardPage {

	private WebDriver driver;

	public HomeDashboardPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//a[text()='Login']")
	private WebElement loginLnk;

	@FindBy(xpath = "//a[text()='Register']")
	private WebElement registerLnk;

	@FindBy(id = "welcome-name")
	private WebElement profieUserNameTxt;

	@FindBy(xpath = "//*[text()='Logout']")
	private WebElement logOutLnk;

	@FindBy(xpath = "//li[contains(text(),'Hi')]")
	private WebElement profileNameTxt;

	@FindBy(xpath = "//img[@alt='Takealot']")
	private WebElement logoImg;

	
	// Method to click on Login Button
	public void clickOnLogin() {
		loginLnk.click();
	}

	// Method to validate Login Success
	public void validateLoginSuccess() {
		ActionUtils.presenceOfElementByXpath(driver, "//li[contains(text(),'Hi')]", 25);
		Assert.assertTrue(profileNameTxt.isDisplayed());
		Assert.assertTrue(logOutLnk.isDisplayed());
		
		Reporter.log("--> User LoggedIn Success ", true);

	}

	// Method to click on Register Button
	public void clickOnRegister() {
		ActionUtils.presenceOfElementByXpath(driver, "//a[text()='Register']", 25);
		registerLnk.click();

		Reporter.log("--> Click on Register Button Success ", true);
	}

	// Method to click on Logout Button
	public void clickOnLogout() {
		ActionUtils.presenceOfElementByXpath(driver, "//*[text()='Logout']", 25);
		logOutLnk.click();
		ActionUtils.presenceOfElementByXpath(driver, "//*[text()='Login']", 25);
		Assert.assertTrue(loginLnk.isDisplayed());

		Reporter.log("--> Logout Success ", true);
	}



}
