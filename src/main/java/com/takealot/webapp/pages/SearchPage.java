package com.takealot.webapp.pages;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;

import com.takealot.webapp.Utils.ActionUtils;

import cucumber.api.DataTable;

public class SearchPage {

	private WebDriver driver;
	
	public SearchPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//button[@type='submit']")
	private WebElement searchBtn;

	@FindBy(name = "department")
	private WebElement allDeptDrpDwn;

	@FindBy(xpath = "//input[@name='search']")
	private WebElement searchTxtFld;

	@FindAll({
			@FindBy(xpath = "//div[@class='product-card product-card-module_product-card_fdqa8']//button[text()='Add to Cart']") })
	private List<WebElement> addToCart;

	@FindBy(xpath = "//div[@class='badge-count']")
	private WebElement itemCount;

	@FindBy(xpath = "//button[@class='badge-button mini-cart-button dark-green badge-icon no-text badge-count']")
	private WebElement checkoutCartBtn;

	// Method to search product based on department
	public void searchProductBasedOnDept(DataTable products, String dept) {

		ActionUtils.presenceOfElementByXpath(driver, "//input[@name='search']", 25);

		List<List<String>> testData = products.raw();
		Iterator iterator = testData.iterator();
		while (iterator.hasNext()) {

			String value = iterator.next().toString().replaceAll("(^\\[|\\]$)", "");
			searchTxtFld.clear();
			searchTxtFld.sendKeys(value);
			ActionUtils.selectDrpDwnByVisibleTxt(driver, allDeptDrpDwn, dept);
			searchBtn.click();
			ActionUtils.waitForElementToLoad(3);
			addToCart.get(0).click();
		}

		ActionUtils.waitForElementToLoad(3);
		Assert.assertEquals(testData.size(), Integer.parseInt(itemCount.getText().toString()));
		
		Reporter.log("--> Searched Products Added to Cart Success ", true);

	}

	// Method to click on Checkout Cart Button
	public void clickOnCheckoutCartBtn() {
		ActionUtils.visibilityOfElementByXapth(driver,
				"//button[@class='badge-button mini-cart-button dark-green badge-icon no-text badge-count']", 25);
		checkoutCartBtn.click();
		
		Reporter.log("--> Click on Checkout Cart Button Success ", true);
	}

}
