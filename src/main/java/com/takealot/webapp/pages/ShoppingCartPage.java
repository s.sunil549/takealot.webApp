package com.takealot.webapp.pages;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;

import com.takealot.webapp.Utils.ActionUtils;

import cucumber.api.DataTable;

public class ShoppingCartPage {
	
private WebDriver driver;
	
	public ShoppingCartPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//*[text()='Shopping Cart']")
	private WebElement shoppingCartTxt;
	
	public WebElement addedProductTxt(String value){
		return driver.findElement(By.xpath("//*[contains(text(),'"+value+"')]"));		
	}
	
	@FindBy(xpath="//*[text()='Cart Summary']")
	private WebElement cartSummarryTxt;
	
	@FindBy(xpath="//a[contains(text(),'Proceed to Checkout')]")
	private WebElement proceedToCheckoutBtn;
	
	
	//Method to validate shopping cart page
	public void validateShoppingCart(DataTable addedProducts){
		
		ActionUtils.presenceOfElementByXpath(driver, "//*[text()='Shopping Cart']", 25);

		Assert.assertTrue(shoppingCartTxt.isDisplayed(), " Shopping Cart Page is not displayed");
		Assert.assertTrue(cartSummarryTxt.isDisplayed(), " Cart Summary is not displayed");
		Assert.assertTrue(proceedToCheckoutBtn.isDisplayed(), " Proceed To checkout button is not displayed");

		List<List<String>> testData = addedProducts.raw();
		Iterator iterator = testData.iterator();
		while (iterator.hasNext()) {

			String value = iterator.next().toString().replaceAll("(^\\[|\\]$)", "");
			Assert.assertTrue(addedProductTxt(value).isDisplayed(), value + " is not displayed");
		}
		
		Reporter.log("--> Validation Shopping Cart Success ", true);
	}

}
