package com.takealot.webapp.pages;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;

import com.takealot.webapp.Utils.ActionUtils;

import cucumber.api.DataTable;

public class RegisterPage  {

	private WebDriver driver;

	public RegisterPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//h1[contains(text(),'Register')]")
	private WebElement registerTxt;

	@FindBy(xpath = "//label[contains(text(),'First Name')]")
	private WebElement firstNameTxt;

	@FindBy(xpath = "//label[contains(text(),'Last Name')]")
	private WebElement lastNameTxt;

	@FindBy(xpath = "//label[contains(text(),'Email')]")
	private WebElement emailTxt;

	@FindBy(xpath = "//label[contains(text(),'Retype Email')]")
	private WebElement retypeEmailTxt;

	@FindBy(xpath = "//label[contains(text(),'Password')]")
	private WebElement pwdTxt;

	@FindBy(xpath = "//label[contains(text(),'Retype Password')]")
	private WebElement reTypePwdTxt;

	@FindBy(xpath = "//label[contains(text(),'Mobile Number')]")
	private WebElement mobNumTxt;

	@FindBy(id = "firstname")
	private WebElement firstNameTxtFld;

	@FindBy(id = "surname")
	private WebElement surNameTxtFld;

	@FindBy(id = "email")
	private WebElement emailTxtFld;

	@FindBy(id = "email2")
	private WebElement reTypeEmailTxtFld;

	@FindBy(id = "password")
	private WebElement pwdeTxtFld;

	@FindBy(id = "password2")
	private WebElement reTypePwdTxtFld;

	@FindBy(id = "telno1")
	private WebElement mobNumTxtFld;

	@FindBy(name = "registerButton")
	private WebElement registerBtn;

	@FindBy(xpath = "//a[@title='Close']")
	private WebElement closeBtn;

	@FindBy(xpath = "//div[@id='welcome']/h3")
	private WebElement welcomeTxt;

	@FindBy(xpath = "//div[@id='welcome']/p[1]")
	private WebElement successTxt1;
	
	@FindBy(xpath="//a[text()='here']")
	private WebElement hereLnk;

	// Method to validate Register User Page
	public void validateReistrationPage() {

		Assert.assertTrue(registerTxt.isDisplayed(), "Register Text is not displayed");
		Assert.assertTrue(firstNameTxt.isDisplayed(), "First Name Text is not displayed");
		Assert.assertTrue(lastNameTxt.isDisplayed(), "Last Name Text is not displayed");
		Assert.assertTrue(emailTxt.isDisplayed(), "Email Text is not displayed");
		Assert.assertTrue(retypeEmailTxt.isDisplayed(), "ReType Email Text is not displayed");
		Assert.assertTrue(pwdTxt.isDisplayed(), "Password Text is not displayed");
		Assert.assertTrue(reTypePwdTxt.isDisplayed(), "ReType Password Text is not displayed");
		Assert.assertTrue(mobNumTxt.isDisplayed(), "Mobile Number Text is not displayed");
		
		Reporter.log("--> Register Screen Validation Success ", true);
	}

	// Method to enter user details to register new user
	public void enterUserDetailsToRegister(DataTable userDetails) {
		
		Random random = new Random();
		int randomNumber = random.nextInt(900) + 100;

		List<List<String>> testData = userDetails.raw();
		String fName = testData.get(0).get(0)+ Integer.toString(randomNumber);
		String lName = testData.get(0).get(1)+Integer.toString(randomNumber);
		String email = testData.get(0).get(2)+Integer.toString(randomNumber)+"@gmail.com";
		String pwd = testData.get(0).get(3);
		String mobNum = testData.get(0).get(4);

		firstNameTxtFld.sendKeys(fName);
		surNameTxtFld.sendKeys(lName);
		emailTxtFld.sendKeys(email);
		reTypeEmailTxtFld.sendKeys(email);
		pwdeTxtFld.sendKeys(pwd);
		reTypePwdTxtFld.sendKeys(pwd);
		mobNumTxtFld.sendKeys(mobNum);
		
		Reporter.log("--> User Details Entered for Registration ", true);
	}

	// Method to click on Register Button
	public void clickOnRegisterBtn() {
		ActionUtils.scrollToElement(driver, registerBtn);
		registerBtn.click();
		Reporter.log("--> Click on Register Button Success ", true);
	}
	
	// Method to validate successful registration
	public void validateRegistrationSuccess(String welcomeMsg, String successMsg){
		
		ActionUtils.presenceOfElementByXpath(driver, "//div[@id='welcome']/h3", 25);
		Assert.assertEquals(welcomeTxt.getText(), welcomeMsg);
		Assert.assertTrue(successTxt1.getText().contains(successMsg));
		hereLnk.click();
		
		try{
			ActionUtils.presenceOfElementByXpath(driver, "//button[text()='Got it']", 25);
			driver.findElement(By.xpath("//button[text()='Got it']")).click();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		Reporter.log("--> Registration Validation Success ", true);
	}

}
