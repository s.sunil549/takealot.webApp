package com.takealot.webapp.Utils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ActionUtils {

	private static WebDriverWait wait;
	
	// Method to scroll to an element in a web page
	public static void scrollToElement(WebDriver dr, WebElement ele) {
		((JavascriptExecutor) dr).executeScript("arguments[0].scrollIntoView(true);", ele);
	}

	// Method to select dropdown list by value
	public static void selectDrpDwnByValue(WebDriver dr, WebElement ele, String value) {
		Select dd = new Select(ele);
		dd.selectByValue(value);
	}

	// Method to select dropdown list by visible text
	public static void selectDrpDwnByVisibleTxt(WebDriver dr, WebElement ele, String value) {
		Select dd = new Select(ele);
		dd.selectByVisibleText(value);
	}

	// Method to check for the visibililty of the element
	public static void visibilityOfElement(WebDriver dr, WebElement ele, int timeUnit) {
		wait = new WebDriverWait(dr, timeUnit);
		wait.until(ExpectedConditions.visibilityOf(ele));
	}

	// Method to check for the visibililty of the element by locator -xpath
	public static void visibilityOfElementByXapth(WebDriver dr, String locator, int timeUnit) {
		wait = new WebDriverWait(dr, timeUnit);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
	}
	
	// Method to check for the visibililty of the element by locator -id
	public static void visibilityOfElementById(WebDriver dr, String locator, int timeUnit) {
		wait = new WebDriverWait(dr, timeUnit);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(locator)));
	}

	// Method to check for the presence of element by locator - xpath
	public static void presenceOfElementByXpath(WebDriver dr, String locator, int timeUnit) {
		wait = new WebDriverWait(dr, timeUnit);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(locator)));
	}

	// Method to check for the presence of element by locator - id
	public static void presenceOfElementById(WebDriver dr, String locator, int timeUnit) {
		wait = new WebDriverWait(dr, timeUnit);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id(locator)));
	}

	// Method to handle staleElementException using WebDriverWait
	public static void handleStaleElementException(WebDriver dr, WebElement element, int timeUnit) {
		wait = new WebDriverWait(dr, timeUnit);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.stalenessOf(element)));
	}
	
	// Method to wait for an element to load
	public static void waitForElementToLoad(int i) {
		try {
			Thread.sleep(i * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
