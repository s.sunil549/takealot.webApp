package com.takealot.api.Utils;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Reporter;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.takealot.api.DTO.CurrentWeatherDataDTO;
import com.takealot.common.CommonCV;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class CurrentWeatherDataAPI extends CommonCV {
	
	private final ObjectMapper mapper = new ObjectMapper();
	private Response response =null;
	private Object json =null;

	/**
	 * @author sunil_s
	 * @description Method to make API call for Current WeatherMap Data By City Name / City Id / ZipCode
	 * @param cityName
	 * @return
	 */
	public CurrentWeatherDataDTO currentWeatherMapByCity(String param,String cityName){
		
		   // Setting of Request URI and BasePath parameter
			RestAssured.baseURI = baseURL;
			RestAssured.basePath = basePath;
			
			// Adding Query parameters for request
			HashMap<String, Object> queryParams = new HashMap<String, Object>();
			queryParams.put(param, cityName);
			queryParams.put("appid", appId);
			
			try{
				response = RestAssured.given().queryParams(queryParams).contentType("application/json").get();
			}catch ( Exception e){
				e.printStackTrace();
				Reporter.log("TestCase: FAIL", true);
			}
			
			CurrentWeatherDataDTO openWeatherAPIDTO = null;
			
			try{
				 mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				 openWeatherAPIDTO =  mapper.readValue(response.asString(), CurrentWeatherDataDTO.class);
				 if(openWeatherAPIDTO == null) throw new IOException("TestCase : FAIL " );			 
			 }catch (IOException e){
				 e.printStackTrace();
				 Reporter.log("TestCase: FAIL", true);			
			 }
			return openWeatherAPIDTO;
	}
	
	/**
	 * @author sunil_s
	 * @description Method to make API call for Current WeatherMap Data By Geographic Coordinates
	 * @param param
	 * @param cityName
	 * @return
	 */
	public CurrentWeatherDataDTO currentWeatherMapByGeographiCoordinates(String param1,String param2){
		
		   // Setting of Request URI and BasePath parameter
			RestAssured.baseURI = baseURL;
			RestAssured.basePath = basePath;
			
			// Adding Query parameters for request
			HashMap<String, Object> queryParams = new HashMap<String, Object>();
			queryParams.put("lon", param1);
			queryParams.put("lat", param2);
			queryParams.put("appid", appId);
			
			try{
				response = RestAssured.given().queryParams(queryParams).contentType("application/json").get();
			}catch ( Exception e){
				e.printStackTrace();
				Reporter.log("TestCase: FAIL", true);
			}
			
			CurrentWeatherDataDTO openWeatherAPIDTO = null;
			
			try{
				 mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				 openWeatherAPIDTO =  mapper.readValue(response.asString(), CurrentWeatherDataDTO.class);
				 if(openWeatherAPIDTO == null) throw new IOException("TestCase : FAIL " );			 
			 }catch (IOException e){
				 e.printStackTrace();
				 Reporter.log("TestCase: FAIL", true);			
			 }
			return openWeatherAPIDTO;
	}
}


