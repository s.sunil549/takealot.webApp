API & UI Automation Framework
=======================

### Objective

The objective of this **Proof of Concept (PoC)** is to provide a functional automation solution for testing business flows within the Takealot Web Application & OpenWeather API. The sequence of activities to achieve the PoC objective is as follows:

    1. Test Suite for Current Weather ( for one location ) feature of Open Weather API.
    2. Test Suite for complex scenarios like Search and Adding products to Cart within Takelot WebApplication.
    3. Integration with BDD-Gherkin to provide more readability on features/ tests
    4. Integration with CI/CD Tools for Daily Run.

### Proposed Solution

Inline to the objective, a robust framework is designed which is combination Page Object Model, Data Driven & BDD Framework using various **Open Source Tools** such as

* Cucumber - Behaviour Driven Development Framework support for Gherkin format
* Selenium WebDriver - WebDriver API used to to perform mobile gestures
* RestAssured - Open source framework for Rest API Testing with BDD format
* Maven - Dependency Management
* TestNG - Test Annotation to organize test suite and parameterization
* Java8 Programming language

The components of framework is structed as shown in below image:

![Framework](/src/main/resources/Screenshots/Framework.png)


### Installations

1. Download and Install JDK8 from the below link
    [https://adoptopenjdk.net/]

2. Download and Install Maven for Commandline from the below link
    [https://maven.apache.org/download.cgi]
    
3. Setting Envirnoment variables JAVA_HOME & M2_HOME

4. Download any IDE (Eclipse or IntelliJ) and install on machine.

5. Install Cucumber Plugin in Eclipse as shown in below link
    [https://github.com/cucumber/cucumber-eclipse-update-site-snapshot]


### Steps for Execution

1. Clone  the repository into any IDE ( Eclipse or IntelliJ )

2. Build the Project using **Maven** to update the dependencies

3. Run the below command from project directory to start execution

For running, API Suite
``` 
    mvn clean install -Drelease.arguments=APISuite.xml 
```
For running, WebApp Suite
``` 
    mvn clean install -Drelease.arguments=WebAppSuite.xml 
```

### Steps for Integrating with Jenkins

1. Login into Jenkins
2. Create a new job as Maven Project / Freestyle Project

    ![JenkinsJob](/src/main/resources/Screenshots/Jenkins_Job.png)
    
3. After then click on the created projected from Jenkins Dashboard and click on “Configure”.
4. In Job Configuration, go to “Build” section and provide the path of pom.xml which has to be executed.

   ![JenkinsSCm](/src/main/resources/Screenshots/Jenkins_SCM.png)
   
   ![JenkinsBuild](/src/main/resources/Screenshots/Jenkins_Build.png)
   
5. And click on “Save” and click on “Build Now” to start the execution on Jenkins.

### Reports after execution

After execution, reports will be generated at **target -> cucumber-reports -> cucumber-html-reports**
and will be as below:

![ReportDetail](/src/main/resources/Screenshots/ReportSummary.png)

![Report](/src/main/resources/Screenshots/ReportDetail.png)
 







